package river;

import java.awt.*;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

public class ScoutGameEngine extends AbstractGameEngine {

    // Private Fields
    private final Item BSCOUT_0 = Item.ITEM_0;
    private final Item ESCOUT_0 = Item.ITEM_1;
    private final Item BSCOUT_1 = Item.ITEM_2;
    private final Item ESCOUT_1 = Item.ITEM_3;
    private final Item BSCOUT_2 = Item.ITEM_4;

    private int crossingCount = 0;

    // Constructor
    public ScoutGameEngine() {
        itemMap = new EnumMap<>(Item.class);
        itemMap.put(BSCOUT_0, new GameObject("B", Location.START, Color.BLUE,true));
        itemMap.put(ESCOUT_0, new GameObject("E", Location.START, Color.GREEN,true));
        itemMap.put(BSCOUT_1, new GameObject("B", Location.START, Color.BLUE,true));
        itemMap.put(ESCOUT_1, new GameObject("E", Location.START, Color.GREEN,true));
        itemMap.put(BSCOUT_2, new GameObject("B", Location.START, Color.BLUE,true));
        boatLocation = Location.START;
    }


    @Override
    public int numberOfItems() {
        return itemMap.size();
    }

    @Override
    public boolean getItemIsDriver(Item item) {
        return true;
    }

    @Override
    public List<Item> getItems() {
        List<Item> items = new ArrayList<>();
        items.add(Item.ITEM_0);
        items.add(Item.ITEM_1);
        items.add(Item.ITEM_2);
        items.add(Item.ITEM_3);
        items.add(Item.ITEM_4);
        return items;
    }

}
